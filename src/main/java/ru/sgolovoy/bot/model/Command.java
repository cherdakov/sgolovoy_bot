package ru.sgolovoy.bot.model;

import java.util.Arrays;
import java.util.Optional;

public enum Command {

    START("/start"),
    CHANGE_NAME("/change_name"),
    EDIT_TRAINING("/edit_training"),
    CREATE_TRAINING("/create_training"),
    SIGN_UP("/sign_up");

    private final String id;

    Command(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public static Optional<Command> findById(String id) {
        return Arrays.stream(Command.values()).filter(c -> c.getId().equals(id)).findFirst();
    }

}
