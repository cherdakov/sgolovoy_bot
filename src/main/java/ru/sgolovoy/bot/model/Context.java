package ru.sgolovoy.bot.model;

import java.util.Optional;

import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

@Getter
@Setter
public class Context {
    Update update;
    boolean isCallback;
    boolean isMessage;
    long tgId;
    String chatId;
    String data;
    String text;
    UserState userState;
    Message message;
    String userName;
    Optional<Command> command;

    public Context(Update update,
                   boolean isCallback,
                   boolean isMessage,
                   long tgId,
                   String chatId,
                   String data,
                   String text,
                   UserState userState,
                   Message message,
                   String userName,
                   Optional<Command> command) {
        this.update = update;
        this.isCallback = isCallback;
        this.isMessage = isMessage;
        this.tgId = tgId;
        this.chatId = chatId;
        this.data = data;
        this.text = text;
        this.userState = userState;
        this.message = message;
        this.userName = userName;
        this.command = command;
    }

}
