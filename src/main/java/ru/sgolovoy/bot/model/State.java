package ru.sgolovoy.bot.model;

public enum State {
    NONE,
    COMMIT_TRAINING,
    MAIN_MENU,
    INTRODUCE,
    PICK_TRAINING_TO_EDIT,
    SET_TRAINING_DATE,
    SET_TRAINING_TIME,
    SET_TRAINER,
    SET_TRAINING_TYPE,
    PICK_TRAINING_TO_SIGN_UP
}
