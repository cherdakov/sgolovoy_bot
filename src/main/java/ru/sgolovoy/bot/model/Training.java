package ru.sgolovoy.bot.model;

import java.time.Instant;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sgolovoy.bot.service.Utils;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "training")
public class Training {
    @Id
    @GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "sequence", sequenceName = "bot.common_sequence")
    private Long id;

    @Column(name = "time")
    Instant time;

    @ManyToOne()
    @JoinColumn(name = "trainer_id", referencedColumnName = "id")
    User trainer;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    TrainingType type;

    @Column(name = "visible")
    Boolean visible = false;

    @ManyToMany(targetEntity = User.class, mappedBy = "trainings")
    private List<User> users;

    public String pretty() {
        String trainerName = trainer == null
                ? "Без тренера"
                : trainer.getName();
        return Utils.pretty(type) + " " + Utils.pretty(time) + ": " + trainerName;
    }
}
