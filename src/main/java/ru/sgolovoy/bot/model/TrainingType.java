package ru.sgolovoy.bot.model;

public enum TrainingType {
    BEACH("Пляжка"),
    CLASSIC("Классика");

    public String name;

    TrainingType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
