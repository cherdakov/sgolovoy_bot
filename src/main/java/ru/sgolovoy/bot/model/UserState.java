package ru.sgolovoy.bot.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user_state")
public class UserState {

    @Id
    @Column(name = "user_id")
    Long userId;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    State state;

    @Column(name = "meta")
    String meta;

    public UserState(Long userId, State state) {
        this.userId = userId;
        this.state = state;
    }
}
