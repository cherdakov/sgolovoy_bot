package ru.sgolovoy.bot.repository;

import java.time.Instant;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sgolovoy.bot.model.Training;

@Repository
@Transactional
public interface TrainingRepository extends JpaRepository<Training, Long> {

    List<Training> findTrainingsByTimeAfterAndVisible(Instant time, boolean visible);
}
