package ru.sgolovoy.bot.service;


import java.util.Collection;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.sgolovoy.bot.model.Context;
import ru.sgolovoy.bot.model.State;
import ru.sgolovoy.bot.telegram.Bot;

@Service
public class ReplyService {

    private final Bot bot;
    private final TrainingService trainingService;
    private final UserStateService userStateService;
    private final UserService userService;

    public ReplyService(Bot bot, TrainingService trainingService, UserStateService userStateService,
                        UserService userService) {
        this.bot = bot;
        this.trainingService = trainingService;
        this.userStateService = userStateService;
        this.userService = userService;
    }

    private InlineKeyboardMarkup getStateKeyboard(State state) {
        switch (state) {
            case SET_TRAINING_DATE:
                return Utils.makeDateKeyboard();
            case SET_TRAINING_TIME:
                return Utils.makeTimeKeyboard();
            case SET_TRAINING_TYPE:
                return Utils.makeTrainingTypeKeyboard();
            case COMMIT_TRAINING:
                return Utils.makeCommitKeyboard();
            case SET_TRAINER:
                return Utils.makeTrainersKeyboard(userService.getTrainers());
            case PICK_TRAINING_TO_EDIT:
                return Utils.makeKeyboard(trainingService.getAllTrainings());
            case PICK_TRAINING_TO_SIGN_UP:
                return Utils.makeKeyboard(trainingService.getAllActualTrainings());
        }
        return null;
    }

    private String getStateMessage(State state) {
        switch (state) {
            case COMMIT_TRAINING:
                return "Подтверждение:";
            case SET_TRAINING_DATE:
                return "Дата:";
            case SET_TRAINING_TIME:
                return "Время:";
            case SET_TRAINING_TYPE:
                return "Тип:";
            case SET_TRAINER:
                return "Тренер:";
            case PICK_TRAINING_TO_EDIT:
                return "Тренировка:";
        }
        return "";
    }

    void reply(Context context) {
        State state = context.getUserState().getState();
        if (context.getUpdate().hasCallbackQuery()) {
            CallbackQuery callbackQuery = context.getUpdate().getCallbackQuery();
            String text = context.getMessage().getText();
            EditMessageText editMessageText = EditMessageText.builder()
                    .text(getNewMessageText(context, state, text))
                    .chatId(context.getChatId())
                    .messageId(callbackQuery.getMessage().getMessageId())
                    .replyMarkup(getStateKeyboard(state))
                    .build();
            bot.editMessageText(editMessageText);
        } else {
            bot.sendMessage(getStateDefaultMessage(context), context.getChatId());
        }
    }

    private String getNewMessageText(Context context, State nextState, String text) {
        String buttonClick = context.getText();
        if (context.isCallback()) {
            Optional<String> buttonText = context.getMessage()
                    .getReplyMarkup()
                    .getKeyboard()
                    .stream()
                    .flatMap(Collection::stream)
                    .filter(b -> b.getCallbackData().equals(context.getData()))
                    .map(InlineKeyboardButton::getText)
                    .findFirst();
            if (buttonText.isPresent()) {
                buttonClick = buttonText.get();
            }
        }
        return text + " " + buttonClick + "\n" + getStateMessage(nextState);
    }

    public SendMessage getStateDefaultMessage(Context context) {
        State state = context.getUserState().getState();
        switch (state) {
            case MAIN_MENU:
                return Utils.mainMenu(context);
            case INTRODUCE:
                return Utils.sendMessage("Представьтесь, пожалуйста");
            case PICK_TRAINING_TO_EDIT:
                return Utils.trainingList(trainingService.getAllTrainings());
            case PICK_TRAINING_TO_SIGN_UP:
                return Utils.trainingList(trainingService.getAllActualTrainings());
            case SET_TRAINING_TYPE:
                return Utils.trainingTypeKeyboard();
            case SET_TRAINING_TIME:
                return Utils.reply("Укажите время");
        }
        return Utils.sendMessage("Тут должно быть главное меню");
    }
}
