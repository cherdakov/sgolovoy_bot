package ru.sgolovoy.bot.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.sgolovoy.bot.model.Command;
import ru.sgolovoy.bot.model.Context;
import ru.sgolovoy.bot.model.State;
import ru.sgolovoy.bot.model.UserState;

@Slf4j
@Service
public class TelegramBotService {

    private final UserService userService;
    private final UserStateService userStateService;
    private final TrainingService trainingService;
    private final QueueService queueService;
    private final ReplyService replyService;

    public TelegramBotService(UserService userService,
                              UserStateService userStateService,
                              TrainingService trainingService,
                              QueueService queueService,
                              ReplyService replyService) {
        this.userService = userService;
        this.userStateService = userStateService;
        this.trainingService = trainingService;
        this.queueService = queueService;
        this.replyService = replyService;
    }

    @Scheduled(fixedDelay = 1)
    void tryTake() {
        Context context = queueService.take();
        boolean isCommand = context.getCommand().isPresent();
        if (!isCommand) {
            tryHandleState(context);
        }
        userStateService.toNextState(context);
        replyService.reply(context);
    }


    boolean tryHandleState(Context context) {
        State state = context.getUserState().getState();
        switch (state) {
            case INTRODUCE:
                userService.updateName(context);
                return true;
            case SET_TRAINING_TYPE:
                context.getUserState().setMeta(trainingService.setType(context).toString());
                return true;
            case SET_TRAINING_TIME:
                trainingService.setTime(context);
                return true;
            case SET_TRAINING_DATE:
                trainingService.setDate(context);
                return true;
            case SET_TRAINER:
                trainingService.setTrainer(context);
                return true;
            case COMMIT_TRAINING:
                trainingService.makeVisibleOrDelete(context);
                return true;
            case PICK_TRAINING_TO_EDIT:
                context.getUserState().setMeta(context.getData());
                return true;
        }
        return false;
    }

}
