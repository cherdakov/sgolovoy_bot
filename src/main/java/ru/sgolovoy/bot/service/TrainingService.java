package ru.sgolovoy.bot.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.sgolovoy.bot.model.Context;
import ru.sgolovoy.bot.model.Training;
import ru.sgolovoy.bot.model.TrainingType;
import ru.sgolovoy.bot.model.User;
import ru.sgolovoy.bot.repository.TrainingRepository;

import static ru.sgolovoy.bot.service.Utils.ZONE_ID;

@Service
public class TrainingService {

    private final TrainingRepository trainingRepository;
    private final UserService userService;


    public TrainingService(TrainingRepository repository, UserService userService) {
        this.trainingRepository = repository;
        this.userService = userService;
    }

    public long createTraining(Context update) {
        Training training = trainingRepository.save(new Training());
        return training.getId();
    }

    public void setTime(Context context) {
        LocalTime time = LocalTime.parse(context.getData());
        long trainingId = Long.parseLong(context.getUserState().getMeta());
        Training training = trainingRepository.getOne(trainingId);
        Instant newTime = time
                .atDate(training.getTime().atZone(ZoneOffset.ofHours(7)).toLocalDate())
                .toInstant(ZONE_ID);
        training.setTime(newTime);
        trainingRepository.save(training);
    }

    public void setTrainer(Context context) {
        long trainingId = Long.parseLong(context.getUserState().getMeta());
        User trainer = userService.getUser(Long.valueOf(context.getData())).get();
        Training training = trainingRepository.getOne(trainingId);
        training.setTrainer(trainer);
        trainingRepository.save(training);
    }

    public void makeVisibleOrDelete(Context context) {
        String data = context.getData();
        long trainingId = Long.parseLong(context.getUserState().getMeta());
        Training training = trainingRepository.getOne(trainingId);
        switch (data) {
            case "create!":
                training.setVisible(true);
                trainingRepository.save(training);
                break;
            case "delete!":
                trainingRepository.delete(training);
                break;
        }
    }

    public Long setType(Context update) {
        TrainingType type = TrainingType.valueOf(update.getData());
        String meta = update.getUserState().getMeta();
        long trainingId = StringUtils.hasText(meta) ? Long.parseLong(meta) : -1;
        Training training = trainingRepository.findById(trainingId).orElse(new Training());
        training.setType(type);
        Training saved = trainingRepository.save(training);
        return saved.getId();
    }

    List<Training> getAllTrainings() {
        return trainingRepository.findAll();
    }

    List<Training> getAllActualTrainings() {
        return trainingRepository.findTrainingsByTimeAfterAndVisible(Instant.now(), true);
    }

    public void setDate(Context context) {
        LocalDate date = LocalDate.parse(context.getData());
        long trainingId = Long.parseLong(context.getUserState().getMeta());
        Training training = trainingRepository.getOne(trainingId);
        Instant newTime = LocalTime.now()
                .atDate(date)
                .toInstant(ZONE_ID);
        training.setTime(newTime);
        trainingRepository.save(training);
    }
}
