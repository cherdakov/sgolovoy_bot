package ru.sgolovoy.bot.service;

import org.springframework.stereotype.Service;
import ru.sgolovoy.bot.model.Command;
import ru.sgolovoy.bot.model.Context;
import ru.sgolovoy.bot.model.State;
import ru.sgolovoy.bot.model.UserState;
import ru.sgolovoy.bot.repository.UserStateRepository;

import static ru.sgolovoy.bot.model.State.COMMIT_TRAINING;
import static ru.sgolovoy.bot.model.State.INTRODUCE;
import static ru.sgolovoy.bot.model.State.MAIN_MENU;
import static ru.sgolovoy.bot.model.State.SET_TRAINER;
import static ru.sgolovoy.bot.model.State.SET_TRAINING_DATE;
import static ru.sgolovoy.bot.model.State.SET_TRAINING_TIME;
import static ru.sgolovoy.bot.model.State.SET_TRAINING_TYPE;

@Service
public class UserStateService {

    private final UserStateRepository userStateRepository;

    public UserStateService(UserStateRepository userStateRepository) {
        this.userStateRepository = userStateRepository;
    }

    public State getNextState(Context context) {
        if (context.getUserState() == null) {
            return INTRODUCE;
        }
        State state = context.getUserState().getState();
        if (context.getCommand().isPresent()) {
            switch (context.getCommand().get()) {
                case START:
                    if (context.getUserName() == null) {
                        return INTRODUCE;
                    } else {
                        return MAIN_MENU;
                    }
                case CHANGE_NAME:
                    return INTRODUCE;
                case EDIT_TRAINING:
                    return State.PICK_TRAINING_TO_EDIT;
                case CREATE_TRAINING:
                    return State.SET_TRAINING_TYPE;
                case SIGN_UP:
                    return State.PICK_TRAINING_TO_SIGN_UP;
            }
        }

        switch (state) {
            case SET_TRAINING_TYPE:
                return SET_TRAINING_DATE;
            case SET_TRAINING_DATE:
                return SET_TRAINING_TIME;
            case SET_TRAINING_TIME:
                return SET_TRAINER;
            case SET_TRAINER:
                return COMMIT_TRAINING;
            case PICK_TRAINING_TO_EDIT:
                return SET_TRAINING_TYPE;
        }
        return MAIN_MENU;
    }

    public void toNextState(Context context) {
        UserState userState = context.getUserState();
        userState.setState(getNextState(context));
        setState(context);
    }

    public void setState(Context context) {
        userStateRepository.save(context.getUserState());
    }

    public UserState getState(Long tgId) {
        return userStateRepository.findById(tgId).orElse(new UserState(tgId, State.NONE));
    }
}
