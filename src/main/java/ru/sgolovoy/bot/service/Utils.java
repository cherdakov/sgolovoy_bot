package ru.sgolovoy.bot.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ru.sgolovoy.bot.model.Command;
import ru.sgolovoy.bot.model.Context;
import ru.sgolovoy.bot.model.Training;
import ru.sgolovoy.bot.model.TrainingType;
import ru.sgolovoy.bot.model.User;

public class Utils {

    public static final ZoneOffset ZONE_ID = ZoneOffset.ofHours(7);

    public static DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("dd.MM HH:mm")
                    .withZone(ZONE_ID)
                    .withLocale(Locale.ROOT);


    public static SendMessage sendMessage(String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(text);
        return sendMessage;
    }

    public static SendMessage reply(String text) {
        return sendMessage(text);
    }

    public static SendMessage trainingList(List<Training> trainings) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Тренировка: ");
        sendMessage.setReplyMarkup(makeKeyboard(trainings));
        return sendMessage;
    }

    public static SendMessage trainingTypeKeyboard() {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Тип тренировки: ");
        sendMessage.setReplyMarkup(makeTrainingTypeKeyboard());
        return sendMessage;
    }

    public static InlineKeyboardMarkup makeKeyboard(List<Training> trainings) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = trainings.stream().
                map(t -> {
                    InlineKeyboardButton button = new InlineKeyboardButton();
                    button.setText(t.pretty());
                    button.setCallbackData(t.getId().toString());
                    return button;
                })
                .map(List::of)
                .collect(Collectors.toList());
        inlineKeyboardMarkup.setKeyboard(keyboard);
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeTrainingTypeKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(List.of(List.of(
                inlineButton("Пляжка", TrainingType.BEACH.name()),
                inlineButton("Классика", TrainingType.CLASSIC.name())
        )));
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeTimeKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(List.of(
                List.of(inlineButton("08:00", "08:00:00")),
                List.of(inlineButton("16:00", "16:00:00")),
                List.of(inlineButton("17:30", "17:30:00")),
                List.of(inlineButton("20:00", "20:00:00"))
        ));
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeCommitKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(List.of(List.of(
                inlineButton("Сохранить", "create!"),
                inlineButton("Удалить", "delete!")
        )));
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeTrainersKeyboard(List<User> trainers) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(List.of(
                trainers.stream()
                        .map(t -> inlineButton(t.getName(), t.getId().toString()))
                        .collect(Collectors.toList())
        ));
        return inlineKeyboardMarkup;
    }


    static InlineKeyboardButton trainingTypeButton(TrainingType trainingType) {
        return inlineButton(trainingType.name(), trainingType.name());
    }

    static InlineKeyboardButton inlineButton(String text, String data) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(data);
        return button;
    }

    public static SendMessage mainMenu(Context context) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("Отлично, " + context.getUserName() + ", выберите пункт меню");
        sendMessage.setReplyMarkup(makeMainMenuKeyboard());
        return sendMessage;
    }

    public static ReplyKeyboardMarkup makeMainMenuKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> buttons =
                Arrays.stream(Command.values())
                        .map(c -> {
                            KeyboardRow keyboardRow = new KeyboardRow();
                            keyboardRow.add(c.getId());
                            return keyboardRow;
                        })
                        .collect(Collectors.toList());

        replyKeyboardMarkup.setKeyboard(buttons);
        return replyKeyboardMarkup;
    }

    public static String pretty(Instant instant) {
        if (instant == null) {
            return "";
        }
        return formatter.format(instant);
    }

    public static InlineKeyboardMarkup makeDateKeyboard() {
        LocalDate today = Instant.now().atZone(ZONE_ID).toLocalDate();
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> buttons = IntStream.range(0, 8)
                .mapToObj(today::plusDays)
                .map(c -> List.of(inlineButton(c.toString(), c.toString())))
                .collect(Collectors.toList());
        inlineKeyboardMarkup.setKeyboard(buttons);
        return inlineKeyboardMarkup;
    }

    public static String pretty(TrainingType type) {
        switch (type) {
            case BEACH:
                return "\uD83C\uDFD6";
            case CLASSIC:
                return "\uD83C\uDFD0";
        }
        return "";
    }
}
